FROM openjdk:8-alpine
ADD target/frontend-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD java -jar frontend-0.0.1-SNAPSHOT.jar