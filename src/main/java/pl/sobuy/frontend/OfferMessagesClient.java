package pl.sobuy.frontend;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sobuy.frontend.model.Offer;

import java.util.List;

@FeignClient(
        value = "offers",
        url = "${offers.endpoint:http://localhost:8088/offers}")
public interface OfferMessagesClient {

    @GetMapping(path = "/")
    List<Offer> getOffers();

    @PostMapping(path = "/")
    Offer addOffer(@RequestBody Offer offer);
}
