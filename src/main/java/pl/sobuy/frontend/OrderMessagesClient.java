package pl.sobuy.frontend;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sobuy.frontend.model.Order;

import java.util.List;

@FeignClient(
        value = "orders",
        url = "${orders.endpoint:http://localhost:8088/orders}")
public interface OrderMessagesClient {

    @GetMapping(path = "/")
    List<Order> getOrders();

    @GetMapping(path = "/{id}")
    Order getOrder(@PathVariable("id") long orderId);

    @PostMapping(path = "/")
    Order addOrder(@RequestBody Order order);

}
