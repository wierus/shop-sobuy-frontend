package pl.sobuy.frontend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Order {

    @Min(value = 1, message = "Offer's id must be 1 or greater")
    private long offerId;

    @Min(value = 1, message = "Your id must be 1 or greater")
    private long clientId;

    @NotNull(message = "Required field")
    @Pattern(regexp = "[0-9]{16,20}", message = "Account number contains between 16 and 20 digits")
    private String clientAccountNumber;

    @Min(value = 1, message = "Quantity of product must be 1 or greater")
    private int quantity;
}
