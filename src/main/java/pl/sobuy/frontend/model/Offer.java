package pl.sobuy.frontend.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
public class Offer {

    @Min(value = 1, message = "Owner's id must be 1 or greater")
    private long ownerId;

    @NotNull(message = "Required field")
    @Pattern(regexp = "[0-9]{16,20}", message = "Account number contains between 16 and 20 digits")
    private String ownerAccountNumber;

    @NotNull(message = "Required field")
    @Size(min = 5, max = 40, message = "Title must have between 5 and 40 characters")
    private String title;

    @NotNull(message = "Required field")
    @Size(min = 15, max = 250, message = "Description must have between 15 and 250 characters")
    private String description;

    @Min(value = 1, message = "Quantity of product must be 1 or greater")
    private int quantity;

    @NotNull(message = "Required field")
    @DecimalMin(value = "0.01", message = "Lowest possible price: 0,01")
    private BigDecimal price;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @NotNull(message = "Date is required!")
    private LocalDateTime endDate;
}