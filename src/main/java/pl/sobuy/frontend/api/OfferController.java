package pl.sobuy.frontend.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sobuy.frontend.model.Offer;
import pl.sobuy.frontend.OfferMessagesClient;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/offers")
public class OfferController {

    private final OfferMessagesClient client;
    private final Logger logger = LoggerFactory.getLogger(OfferController.class);

    @Autowired
    public OfferController(OfferMessagesClient client) {
        this.client = client;
    }

    @PostMapping("/create")
    public String create(@Valid Offer offer, BindingResult result) {
        logger.warn("--------- Method Create - OfferController ------------------- \n value of offer: " + offer.toString() + "\n binding: " + result.toString() + result.getAllErrors());
        validateEndDate(offer, result);
        if (!result.hasErrors()) {
            logger.info("Offer : " + offer.toString());
            client.addOffer(offer);
            return "success";
        }
        return "offerForm";
    }

    @GetMapping("/")
    public String findAll(Model model) {
        logger.warn("--------- Method FindAll - OfferController ------------------- \n value of Model: " + model.toString());
        model.addAttribute("offers", client.getOffers());
        return "offers";
    }

    @GetMapping(path = "/offerForm")
    public String showForm(Model model) {
        logger.warn("--------- Method offerForm - OfferController ------------------- \n value of Model: " + model.toString());
        Offer offer = new Offer();
        model.addAttribute("offer", offer);
        return "offerForm";
    }

    private void validateEndDate(Offer offer, BindingResult result) {
        LocalDateTime now = LocalDateTime.now();
        if (!now.plusMinutes(30).isBefore(offer.getEndDate())) {
            result.rejectValue("endDate", "error.offer", "The sales offer must be active for at least 30 minutes");
        }
        offer.setStartDate(now);
    }
}
