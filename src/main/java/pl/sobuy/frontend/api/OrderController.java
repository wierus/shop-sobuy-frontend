package pl.sobuy.frontend.api;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sobuy.frontend.OrderMessagesClient;
import pl.sobuy.frontend.model.Order;

import javax.validation.Valid;

@Controller
@RequestMapping("/orders")
@RequiredArgsConstructor
class OrderController {

    private final OrderMessagesClient client;

    @PostMapping("/create")
    public String create(@Valid Order order, BindingResult result) {
        if (!result.hasErrors()) {
            client.addOrder(order);
        }
        return "orderForm";
    }

    @GetMapping("/")
    public String findAll(Model model) {
        model.addAttribute("orders", client.getOrders());
        return "orders";
    }

    @GetMapping("/orderForm")
    public String showForm(Model model) {
        model.addAttribute("order", new Order());
        return "orderForm";
    }
}
