# Frontend for shop-sobuy project

Shop SoBuy is a platform for making sales offer, order and settlements of payments.
Project uses Spring Cloud (OpenFeign) to connect to [shop-sobuy-service](https://gitlab.com/wierus/shop-sobuy-service) (service layer), Thymeleaf as a HTML5 template engine and Jenkins for Continous Delivery

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to have installed in order to run project locally

* Java 8
```
To run application go to FrontendApplication class and start it 
```

## Heroku

Project is available on heroku [here](https://sobuy-frontend.herokuapp.com).

## Authors

* **Patryk Stanek**  
e-mail: *pstanek18@gmail.com*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details